# tutorial_dash-plotly_by-datacamp_v01a

Following a tutorial from DataCamp, we'll build a basic dashboard in Python using Dash + Plotly and deploy to (1) localhost and (2) heroku

## Tutorial

### DataCamp: [Dash for Beginners](https://www.datacamp.com/community/tutorials/learn-build-dash-python)

##### _Learn how to build dashboards in Python using Dash._

